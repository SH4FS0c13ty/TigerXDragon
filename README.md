
```
████████╗██╗ ██████╗ ███████╗██████╗     ██╗  ██╗    ██████╗ ██████╗  █████╗  ██████╗  ██████╗ ███╗   ██╗
╚══██╔══╝██║██╔════╝ ██╔════╝██╔══██╗    ╚██╗██╔╝    ██╔══██╗██╔══██╗██╔══██╗██╔════╝ ██╔═══██╗████╗  ██║
   ██║   ██║██║  ███╗█████╗  ██████╔╝     ╚███╔╝     ██║  ██║██████╔╝███████║██║  ███╗██║   ██║██╔██╗ ██║
   ██║   ██║██║   ██║██╔══╝  ██╔══██╗     ██╔██╗     ██║  ██║██╔══██╗██╔══██║██║   ██║██║   ██║██║╚██╗██║
   ██║   ██║╚██████╔╝███████╗██║  ██║    ██╔╝ ██╗    ██████╔╝██║  ██║██║  ██║╚██████╔╝╚██████╔╝██║ ╚████║
   ╚═╝   ╚═╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝

  _______                  _                 _       _____           _        _     _      
 |__   __|                | |               | |     |  __ \         | |      | |   | |     
    | | ___  _ __ __ _  __| | ___  _ __ __ _| |     | |__) |__  _ __| |_ __ _| |__ | | ___ 
    | |/ _ \| '__/ _` |/ _` |/ _ \| '__/ _` | |     |  ___/ _ \| '__| __/ _` | '_ \| |/ _ \
    | | (_) | | | (_| | (_| | (_) | | | (_| |_|     | |  | (_) | |  | || (_| | |_) | |  __/
    |_|\___/|_|  \__,_|\__,_|\___/|_|  \__,_(_)     |_|   \___/|_|   \__\__,_|_.__/|_|\___|
  
      _______                  _       _   _               _______          _ _    _ _
     |__   __|                | |     | | (_)             |__   __|        | | |  (_) |    
        | |_ __ __ _ _ __  ___| | __ _| |_ _  ___  _ __      | | ___   ___ | | | ___| |_   
        | | '__/ _` | '_ \/ __| |/ _` | __| |/ _ \| '_ \     | |/ _ \ / _ \| | |/ / | __|  
        | | | | (_| | | | \__ \ | (_| | |_| | (_) | | | |    | | (_) | (_) | |   <| | |_   
        |_|_|  \__,_|_| |_|___/_|\__,_|\__|_|\___/|_| |_|    |_|\___/ \___/|_|_|\_\_|\__|  
```



## WARNING

The current version is unfinished, cross-platform compatibility is missing for some components but most of the function should work (on Windows at least).<br>
When this version is finished, it may include an auto-translation script using DeepL Pro API that will be compatible with TigerXDragon processing formats.<br>
This will allow you to fully auto-translate the game without having anything to do except choosing your language.<br>
I'm still thinking about including it or not, but I haven't wrote it yet so that's not a problem for the moment.<br>


# TigerXDragon v2
Toradora! Portable Translation Toolkit v2<br>
Created by SH4FS0c13ty
<br><br>

### Requirements
-----

 Windows requirements:
 - Python 3
 - .Net Framework 4

Linux requirements:
 - Python 3
 - curl
 - gambas2
 - libpng-dev, liblcms2-dev
 - p7zip-full
 - wine

Required Python modules:
 - translate-toolkit

 You can install Python modules by using `python -m pip install <package_to_install>`.


<br>



### Instructions
-----

You must NOT install TigerXDragon in a path with spaces (some programs won't work otherwise).<br>
Place the ISO file in the "files/ISO/" path and launch "TigerXDragon.py"<br>
Select the option you want and let it process the ISO and files.<br>
<br>


### License
-----

MIT License (https://opensource.org/licenses/mit-license.php)<br>

Copyright (c) 2019 SH4FS0c13ty<br>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and<br>
associated documentation files (the "Software"), to deal in the Software without restriction,<br>
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,<br>
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,<br>
subject to the following conditions:<br>

The above copyright notice and this permission notice shall be included in all copies or substantial<br>
portions of the Software.<br>

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT<br>
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.<br>
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,<br>
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE<br>
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.<br 
<br>



### Credits
-----

Check the project website to know more about it: https://toradora-fr.tk<br>
Author: SH4FS0c13ty (Twitter: @SH4FS0c13ty, Discord: SH4FS0c13ty#1562, Github: https://github.com/SH4FS0c13ty)<br>
<br>
Thanks to xyz for the "taiga-aisaka" project (https://github.com/xyzz/taiga-aisaka).<br>
Thanks to marcussacana for the "DatWorker" project (https://github.com/marcussacana/Specific-Games/tree/master/Stuff's).<br>
Thanks to 123321mario for his help (http://123321mario.tk/).<br>
Thanks to the french translation team of Toradora! Portable (https://traduction.toradora-fr.tk/).<br>
