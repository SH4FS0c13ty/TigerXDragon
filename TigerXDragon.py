# -*- coding: utf-8 -*-
########################################################################
##                            TigerXDragon                            ##
########################################################################
##       TigerXDragon.py is a part of the TigerXDragon project.       ##
########################################################################
## TigerXDragon.py is the main script that must be executed to run    ##
## the toolkit. You can use both the menu or the command line         ##
## arguments. It allows you to do whatever the toolkit has to do,     ##
## either in automatic or manual mode. Check out the README.md file.  ##
########################################################################
## Shell syntax: TigerXDragon.py [-a] [-i ISO_FILE] [-s STRINGS_DIR]  ##
########################################################################
## Author: SH4FS0c13ty                                                ##
## Copyright: Copyright 2020, TigerXDragon                            ##
## Credits: [SH4FS0c13ty, 123321mario, marcussacana, xyzz]            ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 2.0.0                                                     ##
## Repository: https://gitlab.com/SH4FS0c13ty/TigerXDragon            ##
## Maintainer: SH4FS0c13ty                                            ##
## Email: sh4fs0c13ty@protonmail.com                                  ##
########################################################################

# System modules
import sys, os, types, glob, shutil

# Internal modules

# try:
    # import modules.dependencies
# except ModuleNotFoundError:
    # print("Dependencies module could not be found.")
    # sys.exit(-1)

try:
    from modules.errors import *
    import modules.syscon, modules.extract, modules.repack
except ModuleNotFoundError as e:
    print("An internal TigerXDragon module could not be found.")
    print(e)
    sys.exit(-1)


def banner():
    """
    Clean the console and print the TigerXDragon banner

    :return: None
    :rtype: None
    """

    # Clear console
    modules.syscon.clear_con()
    
    # Set console title
    modules.syscon.set_con_title("TigerXDragon: Toradora! Portable Translation Toolkit v2.0.0")
    
    # Print banner
    print()
    print("████████╗██╗ ██████╗ ███████╗██████╗     ██╗  ██╗    ██████╗ ██████╗  █████╗  ██████╗  ██████╗ ███╗   ██╗")
    print("╚══██╔══╝██║██╔════╝ ██╔════╝██╔══██╗    ╚██╗██╔╝    ██╔══██╗██╔══██╗██╔══██╗██╔════╝ ██╔═══██╗████╗  ██║")
    print("   ██║   ██║██║  ███╗█████╗  ██████╔╝     ╚███╔╝     ██║  ██║██████╔╝███████║██║  ███╗██║   ██║██╔██╗ ██║")
    print("   ██║   ██║██║   ██║██╔══╝  ██╔══██╗     ██╔██╗     ██║  ██║██╔══██╗██╔══██║██║   ██║██║   ██║██║╚██╗██║")
    print("   ██║   ██║╚██████╔╝███████╗██║  ██║    ██╔╝ ██╗    ██████╔╝██║  ██║██║  ██║╚██████╔╝╚██████╔╝██║ ╚████║")
    print("   ╚═╝   ╚═╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝    ╚═╝  ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝")
    print()
    print("Toradora! Portable Translation Toolkit by SH4FS0c13ty")
    print("TigerXDragon v2.0.0 (Codename: Taiga Aisaka) project under MIT License")
    print()

    return


####################################################################
##  Menu generation functions (main menu and ISO detection menu)  ##
####################################################################

def menu_gen(prompt_title, items_list, func_list):
    """
    Generate a text menu with numbers as selection type.
    
    :prompt_title: Prompt title to print
    :type prompt_title: str
    :items_list: List of menu items
    :type items_list: list
    :func_list: Functions assigned to items in menu
    :type func_list: list
    
    :return: Nothing
    :rtype: None
    """
    
    # Check the submitted argument
    if type(items_list) != list or type(func_list) != list:
        raise ArgumentTypeError
    elif len(items_list) != len(func_list):
        raise InvalidLengthError("List length are not the same.")
    
    # Menu loop
    while True:
        # Display the banner and title
        banner()
        
        # Set the pause variable (wait for input after execution)
        pause = True
        
        # Generate the menu and check according functions
        len_items_list = len(items_list)
        for i in range(0, len_items_list):
            if not isinstance(func_list[i], types.FunctionType):
                # Check for submenus
                if type(func_list[i]) != list or len(func_list[i]) != 3:
                    raise ValueError("ValueError: Not a function type.")
            print(" [" + str(i+1) + "]", str(items_list[i]))
        
        # Display exit items with 0 as assigned number
        print()
        print(" [0] Exit")
        print()
    
    
        # Get text in prompt and verify submitted line
        try:
            com = int(input("TigerXDragon" + str(prompt_title) + "> "))
            if com < 0 or com > len(func_list):
                raise ValueError
            else:
                if com == 0:
                    break
                else:
                    # Check for function or submenu and execute according functions
                    if isinstance(func_list[com-1], types.FunctionType):
                        print()
                        func_res = func_list[com-1]()
                        if func_res == "no_pause":
                            pause = False
                    else:
                        menu_gen(func_list[com-1][0], func_list[com-1][1], func_list[com-1][2])
                        pause = False
        except ValueError:
            print("\nYou entered a wrong number. Please choose one of the number above.")
        
        if pause:
            input("\nPress ENTER to continue ...")
        
    return


def detect_iso(folder, message):
    """
    Detect and generate a menu to choose ISO file in folder.
    
    :folder: Folder where ISO files are stored
    :type folder: str
    :message: Message to print
    :type message: str
    
    :return: ISO filename or error code
    :rtype: str or int
    """
    
    # Check folder existence
    if not os.path.isdir(folder):
        raise FileNotFoundError("FileNotFoundError: The folder " + str(folder) + "could not be found.")
    
    # Get ISO files list
    ISO_files = glob.glob(folder + "/*.iso")
    
    # Check ISO files list length
    len_iso_list = len(ISO_files)
    if len_iso_list == 0:
        raise FileNotFoundError("FileNotFoundError: No ISO file found in", str(folder), "folder.")
    elif len_iso_list == 1:
        return ISO_files[0]
    elif len_iso_list >= 2:
        while True:
            banner()
            print(message)
            print("Press CTRL + C to cancel operation.")
            print()
            for i in range(0, len(ISO_files)):
                print(" [" + str(i) + "] ", ISO_files[i].replace("\\", "/"))
            try:
                choice = int(input("\nSelected ISO file: "))
                if choice < 0 or choice > len_iso_list-1:
                    raise ValueError
                print()
                # input("\nPress ENTER to continue ...")
                return ISO_files[choice]
            except ValueError:
                print("\nYou entered a wrong number. Please choose one of the number above.")
                input("\nPress ENTER to continue ...")
    
    return

#############################################################################
##  END OF "Menu generation functions (main menu and ISO detection menu)"  ##
#############################################################################


#### DUMMY FUNCTION ####
def foo():
    """
    Dummy function.
    """
    print("foo")
    return
#### END OF DUMMY FUNCTION ####



#######################################################
##  ISO and DAT extraction initialization functions  ##
#######################################################

def init_extract_iso_dat_1():
    """
    Initialization of ISO and DAT extraction function (ISO and DAT extraction).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        selected_iso = detect_iso("files/ISO/", "Please select the ISO file you want to extract.")
        print("Starting the extraction process ...")
        modules.extract.iso_extract(selected_iso, "data/iso_extracted/")
        print("Copying DAT files ...")
        modules.misc.copy_dat_files("data/iso_extracted/PSP_GAME/USRDIR/", "data/dat_files/", ["first.dat", "resource.dat"])
        modules.extract.dat_extract("data/dat_files/")
        print("Extraction done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_extract_iso_dat_2():
    """
    Initialization of ISO and DAT extraction function (ISO extraction only).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        selected_iso = detect_iso("files/ISO/", "Please select the ISO file you want to extract.")
        print("Starting the extraction process ...")
        modules.extract.iso_extract(selected_iso, "data/iso_extracted/")
        print("Extraction done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_extract_iso_dat_3():
    """
    Initialization of ISO and DAT extraction function (DAT extraction only).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the extraction process ...")
        print("Copying DAT files ...")
        modules.misc.copy_dat_files("data/iso_extracted/PSP_GAME/USRDIR/", "data/dat_files/", ["first.dat", "resource.dat"])
        modules.extract.dat_extract("data/dat_files/")
        print("Extraction done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return

################################################################
##  END OF "ISO and DAT extraction initialization functions"  ##
################################################################


################################################################
##  Strings and pictures extraction initialization functions  ##
################################################################

def init_extract_strings():
    """
    Initialization of strings extraction function (OBJ extraction + TXT2PO conversion).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the extraction process ...")
        modules.extract.extract_strings("data/dat_files/")
        print("Extraction done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_extract_pictures():
    """
    Initialization of pictures extraction function (GIM extraction + GIM2PNG conversion).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the extraction process ...")
        modules.extract.extract_pictures("data/dat_files/")
        print("Extraction done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return

#########################################################################
##  END OF "Strings and pictures extraction initialization functions"  ##
#########################################################################


######################################################
##  ISO and DAT repacking initialization functions  ##
######################################################

def init_repack_iso_dat_1():
    """
    Initialization of ISO and DAT repacking function (ISO and DAT repacking).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the repacking process ...")
        modules.repack.dat_repack("data/dat_files/")
        print("Copying DAT files ...")
        modules.misc.copy_dat_files("data/dat_files/", "data/iso_extracted/PSP_GAME/USRDIR/", ["first.dat", "resource.dat"])
        modules.repack.iso_repack("files/ISO/Toradora! Portable [Translated].iso", "data/iso_extracted/")
        print("Repacking done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_repack_iso_dat_2():
    """
    Initialization of ISO and DAT repacking function (ISO repacking).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the repacking process ...")
        print("Copying DAT files ...")
        modules.misc.copy_dat_files("data/dat_files/", "data/iso_extracted/PSP_GAME/USRDIR/", ["first.dat", "resource.dat"])
        modules.repack.iso_repack("files/ISO/Toradora! Portable [Translated].iso", "data/iso_extracted/")
        print("Repacking done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_repack_iso_dat_3():
    """
    Initialization of ISO and DAT repacking function (DAT repacking).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the repacking process ...")
        modules.repack.dat_repack("data/dat_files/")
        print("Repacking done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_repack_iso_dat_4():
    """
    Initialization of ISO and DAT repacking function (Xdelta patch generation).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        selected_iso = detect_iso("files/ISO/", "Please select the original ISO file used for translation.")
        print("Starting the patch generation process ...")
        modules.misc.create_xdelta_patch(selected_iso, "files/ISO/Toradora! Portable [Translated].iso", "files/ISO/Toradora! Portable [Translated patch].xdelta")
        print("Xdelta patch generation done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return

###############################################################
##  END OF "ISO and DAT repacking initialization functions"  ##
###############################################################


###############################################################
##  Strings and pictures injection initialization functions  ##
###############################################################

def init_inject_strings():
    """
    Initialization of strings injection function (PO2TXT conversion and OBJ patching).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the injection process ...")
        modules.repack.inject_strings("data/dat_files/")
        print("Injection done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return


def init_inject_pictures():
    """
    Initialization of pictures extraction function (GIM extraction + GIM2PNG conversion).
    
    :return: Nothing
    :retype: None
    """
    
    try:
        print("Starting the injection process ...")
        modules.repack.inject_pictures("data/dat_files/")
        print("Injection done!")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return
    
    return

########################################################################
##  END OF "Strings and pictures injection initialization functions"  ##
########################################################################


#########################################
##  Cleaning initialization functions  ##
#########################################

def clean_folders(folders_to_clean, bypass_non_existent_error=False):
    """
    Clean folders in submitted list.
    
    :return: Nothing
    :rtype: None
    """
    
    # Check argument and folders existence
    if type(folders_to_clean) != list:
        raise ArgumentTypeError
    else:
        valid_folders = []
        for x in folders_to_clean:
            if not os.path.isdir(x) and not bypass_non_existent_error:
                raise FileNotFoundError("FileNotFoundError: The following folder could not be found: \"" + x + "\"")
            elif os.path.isdir(x):
                valid_folders.append(x)
    
    # Clean folders
    for x in valid_folders:
        shutil.rmtree(x)
        os.mkdir(x)
    
    return


def clean_workspace():
    """
    Clean workspace function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/iso_extracted/",
            "data/dat_files/",
            "data/strings/obj/import/",
            "data/strings/obj/export/",
            "data/strings/po/import/",
            "data/strings/po/export/",
            "data/strings/txt/import/",
            "data/strings/txt/export/",
            "data/pictures/gim/import/",
            "data/pictures/gim/export/",
            "data/pictures/png/import/",
            "data/pictures/png/export/"
        ]
        choice = input("Are you sure you want to clean the whole workspace (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning the workspace ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_folders_1():
    """
    Clean import/export folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/strings/obj/import/",
            "data/strings/obj/export/",
            "data/strings/po/import/",
            "data/strings/po/export/",
            "data/strings/txt/import/",
            "data/strings/txt/export/",
            "data/pictures/gim/import/",
            "data/pictures/gim/export/",
            "data/pictures/png/import/",
            "data/pictures/png/export/"
        ]
        choice = input("Are you sure you want to clean all the import/export folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all import/export folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_folders_2():
    """
    Clean import folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/strings/obj/import/",
            "data/strings/po/import/",
            "data/strings/txt/import/",
            "data/pictures/gim/import/",
            "data/pictures/png/import/"
        ]
        choice = input("Are you sure you want to clean all the import folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all import folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_folders_3():
    """
    Clean export folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/strings/obj/export/",
            "data/strings/po/export/",
            "data/strings/txt/export/",
            "data/pictures/gim/export/",
            "data/pictures/png/export/"
        ]
        choice = input("Are you sure you want to clean all the export folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all export folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_strings_folders_1():
    """
    Clean strings import/export folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/strings/obj/import/",
            "data/strings/obj/export/",
            "data/strings/po/import/",
            "data/strings/po/export/",
            "data/strings/txt/import/",
            "data/strings/txt/export/"
        ]
        choice = input("Are you sure you want to clean all the strings import/export folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all strings import/export folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_strings_folders_2():
    """
    Clean strings import folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/strings/obj/import/",
            "data/strings/po/import/",
            "data/strings/txt/import/"
        ]
        choice = input("Are you sure you want to clean all the strings import folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all strings import folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_strings_folders_3():
    """
    Clean strings export folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/strings/obj/export/",
            "data/strings/po/export/",
            "data/strings/txt/export/"
        ]
        choice = input("Are you sure you want to clean all the strings export folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all strings export folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_pictures_folders_1():
    """
    Clean pictures import/export folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/pictures/gim/import/",
            "data/pictures/gim/export/",
            "data/pictures/png/import/",
            "data/pictures/png/export/"
        ]
        choice = input("Are you sure you want to clean all the pictures import/export folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all pictures import/export folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_pictures_folders_2():
    """
    Clean pictures import folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/pictures/gim/import/",
            "data/pictures/png/import/"
        ]
        choice = input("Are you sure you want to clean all the pictures import folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all pictures import folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_pictures_folders_3():
    """
    Clean pictures export folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/pictures/gim/export/",
            "data/pictures/png/export/"
        ]
        choice = input("Are you sure you want to clean all the pictures export folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning all pictures export folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_iso_dat_folders_1():
    """
    Clean ISO and DAT extraction folders function.
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/iso_extracted/",
            "data/dat_files/"
        ]
        choice = input("Are you sure you want to clean the ISO and DAT extraction folders (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning the ISO and DAT extraction folders ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return


def clean_iso_dat_folders_2():
    """
    Clean ISO and DAT extraction folders function (ISO extraction folder only).
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/iso_extracted/"
        ]
        choice = input("Are you sure you want to clean the ISO extraction folder (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning the ISO extraction folder ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return

def clean_iso_dat_folders_3():
    """
    Clean ISO and DAT extraction folders function (DAT extraction folder only).
    
    :return: Nothing
    :rtype: None
    """
    
    try:
        attended_response = ["y", "yes"]
        dirs_to_clean = [
            "data/dat_files/"
        ]
        choice = input("Are you sure you want to clean the DAT extraction folder (y/n)? ")
        if choice.lower() in attended_response:
            print("Cleaning the DAT extraction folder ...")
            clean_folders(dirs_to_clean, True)
            print("Folders cleaned!")
        else:
            print("Cleaning canceled.")
    except KeyboardInterrupt:
        return
    except Exception as e:
        print(e)
        return

##################################################
##  END OF "Cleaning initialization functions"  ##
##################################################


#####################################
##  About/Licenses/Help functions  ##
#####################################

def show_about_section():
    """
    Show the about section.

    :return: None
    :rtype: None
    """

    banner()
    print("First, let me tell you a story:")
    print()
    print("This is the story of a man who was discovering animes. He was new in that world,")
    print("so he had to ask his friends about what he should watch first. They gave the")
    print("man a list of animes in which appeared No Game No Life, Kill la Kill, Kokoro")
    print("Connect, Violet Evergarden and, the greatest anime of all times, Toradora!.")
    print("Just after he finished he was done watching the anime, he couldn't believe that")
    print("it was the end. So he searched as hard as he could on the Internet for a second")
    print("season, but he couldn't find one. The man was so sad, he wanted to keep trying,")
    print("but his friend told him: \"No, there is no 2nd season, and this is unlikely that")
    print("it will ever exist  ...\". However, while he was looking for this non-existent")
    print("sequel, he discovered a PSP game called \"Toradora! Portable\". What a strange")
    print("name for a game, isn't it? The man didn't know shit about PSP games, but he")
    print("downloaded it along with PPSSPP (PSP Emulator for PC) and played the game.")
    print("Did I mention the man was french? It doesn't matter ... Oh wait ... Actually,")
    print("it does matter. The man watched Toradora! with japanese audio and french")
    print("subtitles and he liked it that way. He was disappointed to see that no one")
    print("created a french version of the Toradora! Portable game but he was quite happy")
    print("there was an english version because it's not always the case.")
    print("This is when the ToradoraFR and TigerXDragon projects started.")
    print()
    print("ToradoraFR is the french translation project of Toradora! Portable and")
    print("TigerXDragon is the toolkit used to get and patch in-game files.")
    print("TigerXDragon is now an automatic toolkit capable of patching the full game in a")
    print("matter of minutes. It can even be used by a 6 years old!")
    print()
    print("And that's the whole story, feel free to check out the ToradoraFR project!")
    print()
    print("The TigerXDragon repository is available on Gitlab at:")
    print("https://gitlab.com/SH4FS0c13ty/TigerXDragon")
    print()
    print("The ToradoraFR project website is available at:")
    print("https://toradora-fr.tk")
    print()

    return


def show_license_section():
    """
    Show the license section.

    :return: None
    :rtype: None
    """

    banner()
    print("The whole project is under MIT License. However, some parts of the toolkit")
    print("use other licenses listed below.")
    print()
    print(" - TigerXDragon (MIT License)")
    print(" - 7-Zip (GNU LGPL, unRAR license restriction, BSD 3-clause License)")
    print(" - Curl (Curl License inspired from MIT License)")
    print(" - GimConv (Sony License)")
    print(" - Gpda_handler (MIT License)")
    print(" - Gzip (GPL)")
    print(" - Mkisofs (CDDL)")
    print(" - Modseekmap (MIT License)")
    print(" - OBJEditor (MIT License)")
    print(" - PARAM.SFO Editor (Unknown license)")
    print(" - Pngquant (GPLv3 license)")
    print(" - Xdelta (Apache v2 License)")
    print()
    print("All licenses are available for reading in the \"data/licenses/\" folder.")
    print()

    return


def show_help_section():
    """
    Show the help section.

    :return: None
    :rtype: None
    """

    banner()
    print("TigerXDragon is an automated toolkit to translate the \"Toradora! Portable\" PSP game.")
    print()
    print("In order to make it work correctly, you should have installed all the required dependencies")
    print("and you must put files in the according folders.")
    print()
    print("Files and folders explanations:")
    print(" - The \"data/\" and \"modules/\" folders are internal TigerXDragon folders, therefore you must NOT touch them")
    print("   manually or any elements (files and folders) inside themselves.")
    print(" - The \"files/\" folder is an user folder, you must not touch the directory tree but can put files in the according")
    print("   folders.")
    print(" - As everything is handled by TigerXDragon, you should execute the main \"TigerXDragon.py\" only to make it work.")
    print("   Any other module will not work on its own (except for the db_handler.py module).")
    print(" - Do NOT modify the export tree structure when importing.")
    print()
    print("Basic scenario (I want to extract the strings, translate them then put them back and rebuild an ISO):")
    print(" - Put the original ISO in \"files/ISO/\"")
    print(" - Start the \"TigerXDragon.py\" module")
    print(" - Enter \"1\" (Auto processes)")
    print(" - Enter \"1\" (ISO and DAT extraction + strings extraction)")
    print(" - Take the string files from \"files/strings/export/\"")
    print(" - Translate the strings using any translation platform that support PO files")
    print(" - Put the translated files into \"files/strings/import/\"")
    print(" - Enter \"4\" (ISO and DAT extraction + PO injection + repack)")
    print(" - Accept to clean the ISO and DAT extraction folder")
    print(" - Take the translated ISO file from \"files/ISO/Toradora! Portable [Translated].iso\"")
    print()
    print("You can execute all the tasks manually from the main menu, the process will not change:")
    print(" - Extract the ISO")
    print(" - Extract the DAT files")
    print(" - Extract the strings (and/or pictures)")
    print(" - Inject translated strings (and/or pictures)")
    print(" - Repack the DAT and ISO files")
    print()
    print("Which folders should the user interact with?")
    print(" - \"files/ISO/\": ISO files (original and translated) and Xdelta patch")
    print(" - \"files/strings/import/\": Translated PO files to inject (must be put by the user)")
    print(" - \"files/strings/export/\": Original PO files from the ISO")
    print(" - \"files/pictures/*/\": Same as strings but for pictures")
    print()
    print("If TigerXDragon is taking too much space but you don't want to reinstall it, you")
    print("can use the \"[8] Clean workspace (Internal files)\" option.")
    print()
    print("If you still need more help or want to get into the code, feel free to contact")
    print("me via Discord (SH4FS0c13ty#1562) or Twitter (@SH4FS0c13ty).")
    print()

    return

##############################################
##  END OF "About/Licenses/Help functions"  ##
##############################################


###############################################
##  Auto processes initialization functions  ##
###############################################

def auto_process_1():
    """
    Auto process initialisation function (ISO and DAT extraction + strings extraction).
    
    :return: Nothing
    :rtype: None
    """
    
    dirs_to_clean = [
        "data/iso_extracted/",
        "data/dat_files/",
        "data/strings/obj/export/",
        "data/strings/po/export/",
        "data/strings/txt/export/"
    ]
    clean_folders(dirs_to_clean)
    init_extract_iso_dat_1()
    init_extract_strings()
    
    return


def auto_process_2():
    """
    Auto process initialisation function (ISO and DAT extraction + pictures extraction).
    
    :return: Nothing
    :rtype: None
    """
    
    dirs_to_clean = [
        "data/iso_extracted/",
        "data/dat_files/",
        "data/pictures/gim/export/",
        "data/pictures/png/export/"
    ]
    clean_folders(dirs_to_clean)
    init_extract_iso_dat_1()
    init_extract_pictures()
    
    return


def auto_process_3():
    """
    Auto process initialisation function (ISO and DAT extraction + strings and pictures extraction).
    
    :return: Nothing
    :rtype: None
    """
    
    dirs_to_clean = [
        "data/iso_extracted/",
        "data/dat_files/",
        "data/strings/obj/export/",
        "data/strings/po/export/",
        "data/strings/txt/export/",
        "data/pictures/gim/export/",
        "data/pictures/png/export/"
    ]
    clean_folders(dirs_to_clean)
    init_extract_iso_dat_1()
    init_extract_strings()
    init_extract_pictures()
    
    return


def auto_process_4():
    """
    Auto process initialisation function (ISO and DAT extraction + PO injection + repack).
    
    :return: Nothing
    :rtype: None
    """
    
    dirs_to_clean = [
        "data/iso_extracted/",
        "data/dat_files/",
        "data/strings/obj/import/",
        "data/strings/po/import/",
        "data/strings/txt/import/"
    ]
    clean_folders(dirs_to_clean)
    init_extract_iso_dat_1()
    init_inject_strings()
    
    return


def auto_process_5():
    """
    Auto process initialisation function (ISO and DAT extraction + GIM injection + repack).
    
    :return: Nothing
    :rtype: None
    """
    
    dirs_to_clean = [
        "data/iso_extracted/",
        "data/dat_files/",
        "data/pictures/gim/import/",
        "data/pictures/png/import/"
    ]
    clean_folders(dirs_to_clean)
    init_extract_iso_dat_1()
    init_inject_pictures()
    
    return


def auto_process_6():
    """
    Auto process initialisation function (ISO and DAT extraction + PO and GIM injection + repack).
    
    :return: Nothing
    :rtype: None
    """
    
    dirs_to_clean = [
        "data/iso_extracted/",
        "data/dat_files/",
        "data/strings/obj/import/",
        "data/strings/po/import/",
        "data/strings/txt/import/",
        "data/pictures/gim/import/",
        "data/pictures/png/import/"
    ]
    clean_folders(dirs_to_clean)
    init_extract_iso_dat_1()
    init_inject_strings()
    init_inject_pictures()
    
    return

########################################################
##  END OF "Auto processes initialization functions"  ##
########################################################


def main():
    print("Full script not implemented yet.")
    
    try:
        # Display banner
        banner()
        
        # Print main menu
        main_menu = [
            "Auto processes (Auto extractions and injections)" + "  [DONE / NOT TESTED YET]" + "\n",
        
            "Extract (ISO and DAT files)" + "  [DONE]",
            "Extract (PO strings)" + "  [DONE]",
            "Extract (GIM images)" + "  [DONE]" + "\n",
            
            "Inject (PO strings)" + "  [DONE]",
            "Inject (GIM images)" + "  [DONE]" + "\n",
            
            "Repack (DAT and ISO files)" + "  [DONE]" + "\n",
            
            "Clean workspace (Internal files)" + "  [DONE]",
            "Edit PARAM.SFO file (Before repack operation)" + "  [NOT IMPLEMENTED]" + "\n",
            
            "About, licenses, help" + "  [DONE]"
        ]
        
        main_menu_func = [
            [
                "/auto_processes", 
                [
                    "ISO and DAT extraction + strings extraction (PO files)",
                    "ISO and DAT extraction + pictures extraction (GIM files)",
                    "ISO and DAT extraction + strings and pictures extraction (PO and GIM files)\n",
                    "ISO and DAT extraction + PO injection + repack",
                    "ISO and DAT extraction + GIM injection + repack",
                    "ISO and DAT extraction + PO and GIM injection + repack\n"
                ],
                [
                    auto_process_1,
                    auto_process_2,
                    auto_process_3,
                    auto_process_4,
                    auto_process_5,
                    auto_process_6
                ]
            ],
            
            [
                "/extract",
                [
                    "Extract ISO and DAT files",
                    "Extract ISO only",
                    "Extract DAT files only"
                ],
                [
                    init_extract_iso_dat_1,
                    init_extract_iso_dat_2,
                    init_extract_iso_dat_3
                ]
            ],
            
            init_extract_strings,
            init_extract_pictures,
            
            init_inject_strings,
            init_inject_pictures,
            
            [
                "/repack",
                [
                    "Repack ISO and DAT files",
                    "Repack ISO only", "Repack DAT files only\n",
                    "Generate an Xdelta patch"
                ],
                [
                    init_repack_iso_dat_1,
                    init_repack_iso_dat_2,
                    init_repack_iso_dat_3,
                    init_repack_iso_dat_4
                ]
            ],
            
            [
                "/clean",
                [
                    "Clean the whole workspace\n",
                    "Clean all internal import/export folders",
                    "Clean internal strings import/export folders",
                    "Clean internal pictures import/export folders\n",
                    "Clean ISO and DAT extraction folders"
                ],
                [
                    clean_workspace,
                    [
                        "/clean/all_import_export",
                        [
                            "Clean both internal import/export folders",
                            "Clean internal import folders only",
                            "Clean internal export folders only"
                        ],
                        [
                            clean_folders_1,
                            clean_folders_2,
                            clean_folders_3
                        ]
                    ],
                    [
                        "/clean/strings_import_export",
                        [
                            "Clean both internal strings import/export folders",
                            "Clean internal strings import folders only",
                            "Clean internal strings export folders only"
                        ],
                        [
                            clean_strings_folders_1,
                            clean_strings_folders_2,
                            clean_strings_folders_3
                        ]
                    ],
                    [
                        "/clean/pictures_import_export",
                        [
                            "Clean both internal pictures import/export folders",
                            "Clean internal pictures import folders only",
                            "Clean internal pictures export folders only"
                        ],
                        [
                            clean_pictures_folders_1,
                            clean_pictures_folders_2,
                            clean_pictures_folders_3
                        ]
                    ],
                    [
                        "/clean/iso_dat_extracted",
                        [
                            "Clean both ISO and DAT extraction folders",
                            "Clean ISO extraction folder only",
                            "Clean DAT extraction folder only"
                        ],
                        [
                            clean_iso_dat_folders_1,
                            clean_iso_dat_folders_2,
                            clean_iso_dat_folders_3
                        ]
                    ]
                ]
            ],
            
            foo,
            
            [
                "/about-licenses-help",
                [
                    "About the toolkit and the project",
                    "Licenses",
                    "Help"
                ],
                [
                    show_about_section,
                    show_license_section,
                    show_help_section
                ]
            ]
        ]
        
        
        menu_gen("", main_menu, main_menu_func)
        
        
        
    except KeyboardInterrupt:
        print("\nExiting TigerXDragon ...")
    # except Exception as e:
        # print(e)
        # return
    
    
    return


if __name__ == "__main__":
    main()
