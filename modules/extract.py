# -*- coding: utf-8 -*-
########################################################################
##                            TigerXDragon                            ##
########################################################################
##             extract.py is a part of the PyTTP project.             ##
########################################################################
## extract.py contains all functions used to extract the ISO and .dat ##
## files. It needs to access data/tools/* containing 7-Zip and        ##
## tenoritool to extract everything.                                  ##
########################################################################
## Shell syntax: This script has no shell syntax.                     ##
########################################################################
## Author: SH4FS0c13ty                                                ##
## Copyright: Copyright 2020, TigerXDragon                            ##
## Credits: [SH4FS0c13ty, 123321mario, marcussacana, xyzz]            ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 2.0.0                                                     ##
## Maintainer: SH4FS0c13ty                                            ##
## Email: sh4fs0c13ty@protonmail.com                                  ##
########################################################################

# System modules
import sys, os, glob, shutil, translate.convert.txt2po

# Internal modules
from modules.errors import *
import modules.misc


#######################################
##  ISO and DAT extraction functions  #
#######################################

def iso_extract(iso_file, extraction_folder):
    """
    Extract the ISO file into the extraction folder.

    :iso_file: ISO file to extract
    :type iso_file: str
    :extraction_folder: Folder where the ISO will be extracted
    :type extraction_folder: str

    :return: Nothing
    :rtype: None
    """

    # Check existence of ISO file and extraction folder (and checking if it is an ISO file)
    try:
        if not os.path.isfile(iso_file):
            raise FileNotFoundError
        elif iso_file.split(".")[-1].lower() != "iso":
            raise InvalidFormatError("File extension is not \".iso\".")
        elif not os.path.isdir(extraction_folder):
            os.mkdir(extraction_folder)
        elif os.path.isdir(extraction_folder):
            # Let the user choose to overwrite files or not
            if os.listdir(extraction_folder) != []:
                clean_dir_choice = modules.misc.not_empty_folder_choice(extraction_folder)
                if clean_dir_choice:
                    shutil.rmtree(extraction_folder)
                    os.mkdir(extraction_folder)
                    print("Folder cleaned!")
                else:
                    raise OSError("OSError: Directory not empty.")
    except AttributeError:
        raise InvalidArgumentType
    
    print("Extracting ISO file ...")
    
    # Extract the ISO file
    program_path = "data/tools/7z/7z"
    args = ["x", "-y", "-o" + extraction_folder, iso_file]
    modules.misc.execute(program_path, args)
    # modules.misc.execute(program_path, args, "STDOUT") # Show full program output

    # Check extracted file list existence
    if not os.path.isfile(extraction_folder + "/UMD_DATA.BIN") or not os.path.isdir(extraction_folder + "/PSP_GAME/"):
        raise FileNotFoundError("FileNotFoundError: Some files could not be extracted.")

    return


def dat_extract(dat_folder):
    """
    Extract all .dat files in submitted folder.
    
    :dat_folder: DAT extraction folder
    :type dat_folder: str
    
    :return: Nothing
    :rtype: None
    """
    
    # Check folder existence
    if not os.path.isdir(dat_folder):
        os.mkdir(dat_folder)
    elif os.path.isdir(dat_folder):
        # Let the user choose to overwrite files or not
        if os.listdir(dat_folder) != [] and os.listdir(dat_folder) != ["first.dat", "resource.dat"]:
            clean_dir_choice = modules.misc.not_empty_folder_choice(dat_folder)
            if clean_dir_choice:
                os.mkdir("../tmp/")
                modules.misc.copy_dat_files(dat_folder, "../tmp/", ["first.dat", "resource.dat"])
                shutil.rmtree(dat_folder)
                os.mkdir(dat_folder)
                modules.misc.copy_dat_files("../tmp/", dat_folder, ["first.dat", "resource.dat"])
                shutil.rmtree("../tmp/")
                print("Folder cleaned!")
            else:
                raise OSError("OSError: Directory not empty.")
    
    print("Extracting DAT files ...")
    
    for x in glob.glob(dat_folder.replace("\\", "/") + "*.dat"):
        x = x.replace("\\", "/")
        # Extract the .dat file
        program_path = "data/tools/gpda_handler/gpda_handler"
        args = [x.split(dat_folder)[-1]]
        modules.misc.execute(program_path, args, "DEVNULL", dat_folder)
        # modules.misc.execute(program_path, args, "STDOUT", dat_folder) # Show full program output
        if not os.path.isfile(x + ".txt"):
            raise FileNotFoundError("FileNotFoundError: Some files could not be extracted.")
    
    return

################################################
##  END OF "ISO and DAT extraction functions"  #
################################################


################################################
##  Strings and pictures extraction functions  #
################################################

def txt2po_list(all_files, dest_folder):
    """
    Convert TXT files to PO files.

    :all_files: List of TXT files to convert
    :type all_files: list
    :dest_folder: Destination folder for converted files
    :type dest_folder:
    
    :return: Nothing
    :rtype: None
    """

    # Check arguments
    if type(all_files) != list:
        raise ArgumentTypeError
    elif not os.path.isdir(dest_folder):
        raise FileNotFoundError("FileNotFoundError: The destination folder could not be found.")

    # Convert TXT to PO
    for x in all_files:
        x = x.replace("\\", "/")
        # print("Converting", x, "...") # Show full program output
        if not os.path.isfile(x):
            raise FileNotFoundError("FileNotFoundError: TXT file not found \"" + x + "\".")
        translate.convert.txt2po.main(["--progress=none", "--errorlevel=exception", x, dest_folder + x.split("/")[-1].rstrip(".txt") + ".po"])
        sys.stdout.flush()
        if not os.path.isfile(dest_folder + x.split("/")[-1].rstrip(".txt") + ".po"):
            raise FileNotFoundError("FileNotFoundError: Error while translating \"" + x + "\"")
    
    return


def extract_strings(dat_folder):
    """
    Extract the strings from extracted DAT files.

    :dat_folder: DAT extraction folder
    :type dat_folder: str
    
    :return: Nothing
    :rtype: None
    """

    # Check folders existence
    if not os.path.isdir(dat_folder + "/resource/"):
        raise FileNotFoundError("FileNotFoundError: DAT files have not been extracted.")
    else:
        str_dirs = ["data/strings/obj/", "data/strings/po/", "data/strings/txt/", "files/strings/"]
        str_subdirs = ["export/"]
        dirs_to_clean = []
        for x in str_dirs:
            if not os.path.isdir(x):
                os.mkdir(x)
            for y in str_subdirs:
                if not os.path.isdir(x + y):
                    os.mkdir(x + y)
                elif os.listdir(x + y) != []:
                    dirs_to_clean.append(x + y)
        if dirs_to_clean != []:
            clean_dir_choice = modules.misc.not_empty_folder_choice("data/strings/*/export/", True)
            if clean_dir_choice:
                for x in dirs_to_clean:
                    shutil.rmtree(x)
                    os.mkdir(x)
                print("Folders cleaned!")
            else:
                raise OSError("OSError: Directory not empty.")
        
    print("Getting compressed string files ...")
    
    # Getting all .obj.gz files
    lst_path = dat_folder + "/resource/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.obj.gz"))]
    
    print("Copying compressed string files ...")
    
    # Copying compressed string files
    for i in range(0, len(all_files)):
        shutil.copy(all_files[i], "data/strings/obj/export/")
        all_files[i] = "data/strings/obj/export/" + all_files[i].replace("\\", "/").split("/")[-1]
    
    print("Removing invalid files ...")
    
    # List of invalid OBJ files (or that should not be translated)
    invalid_obj = [
        "_0011AAA0.obj",
        "_0012AAA0.obj",
        "_0021aaa0.obj",
        "_0022aaa0.obj",
        "_0031aaa0.obj",
        "_0033aaa0.obj",
        "_0035aaa0.obj",
        "_a041aaa0.obj",
        "_a043aaa0.obj",
        "_a052aaa0.obj",
        "_a054aaa0.obj",
        "_a055aaa0.obj",
        "_a192aaa0.obj",
        "_a291aaa0.obj",
        "_a361aaa0.obj",
        "_a362aaa0.obj",
        "_b041aaa0.obj",
        "_b045aaa0.obj",
        "_b062aaa0.obj",
        "_b192aaa0.obj",
        "_c041aaa0.obj",
        "_c042aaa0.obj",
        "_c051aaa0.obj",
        "_c053aaa0.obj",
        "_c054aaa0.obj",
        "_c056aaa0.obj",
        "_c061aaa0.obj",
        "_c064aaa0.obj",
        "_c065aaa0.obj",
        "_c072aaa0.obj",
        "_d021aaa0.obj",
        "STARTPOINT.obj"
    ]
    
    # Remove invalid OBJ files
    for x in invalid_obj:
        os.remove("data/strings/obj/export/" + x + ".gz")
    
    print("Extracting string files ...")
    
    # Extract OBJ files from GZ archives
    for x in all_files:
        program_path = "data/tools/gzip/gzip"
        args = ["-d", "-f", "-q", x]
        modules.misc.execute(program_path, args)
    
    # Getting all .obj extracted files
    lst_path = "data/strings/obj/export/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.obj"))]
    
    print("Converting OBJ files to TXT files ...")
    
    # Convert OBJ files to TXT files
    for x in all_files:
        x = x.replace("\\", "/")
        # print("Converting", str(x), "...")
        program_path = "data/tools/objeditor/OBJEGUI.exe"
        args = ["-export", x, "data/strings/obj/export/" + x.split("/")[-1].rstrip(".obj") + ".txt"]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Copying all the TXT files ...")
    
    # Getting all .txt files
    lst_path = "data/strings/obj/export/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.txt"))]
    
    # Copying TXT string files
    for i in range(0, len(all_files)):
        # print("Copying", all_files[i], "...") # Show full program output
        shutil.move(all_files[i], "data/strings/txt/export/")
        all_files[i] = "data/strings/txt/export/" + all_files[i].replace("\\", "/").split("/")[-1]
    
    # Print to inform of strings conversion
    print("Converting strings into PO format ...")

    # Convert TXT files to PO files
    txt2po_list(all_files, "data/strings/po/export/")
    
    print("Exporting strings into user's folder ...")
    
    # Get all converted PO files
    lst_path = "data/strings/po/export/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.po"))]
    
    # Copy converted PO files to user's export folder
    for i in range(0, len(all_files)):
        # print("Copying", x, "...") # Show full program output
        shutil.copy(all_files[i], "files/strings/export/")

    print("Extracting and exporting special files ...")

    # Get all first.dat translatable files
    all_files = ["charaname.txt", "ItemList.csv", "placename.txt", "utf8.txt", "utf16.txt"]
    if not os.path.isdir("files/strings/export/first/"):
        os.mkdir("files/strings/export/first/")
    for x in all_files:
        # print("Copying", x, "...") # Show full program output
        shutil.copy("data/dat_files/first/text.dat/" + x + ".gz", "files/strings/export/first/" + x + ".gz")
        # print("Extracting", x, "...") # Show full program output
        # Extract file from GZ archive
        program_path = "data/tools/gzip/gzip"
        args = ["-d", "-f", "-q", "files/strings/export/first/" + x + ".gz"]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
        # Check for extracted file
        if not os.path.isfile("files/strings/export/first/" + x):
            raise FileNotFoundError("FileNotFoundError: An error occured while extracting special files.")
    
    return


def extract_pictures(dat_folder):
    """
    Extract the pictures from extracted DAT files.
    
    :dat_folder: DAT extraction folder
    :type dat_folder: str
    
    :return: Nothing
    :rtype: None
    """
    
    # Check folders existence
    if not os.path.isdir(dat_folder + "/resource/"):
        raise FileNotFoundError("FileNotFoundError: DAT files have not been extracted.")
    else:
        str_dirs = ["data/pictures/gim/", "data/pictures/png/", "files/pictures/"]
        str_subdirs = ["export/"]
        dirs_to_clean = []
        for x in str_dirs:
            if not os.path.isdir(x):
                os.mkdir(x)
            for y in str_subdirs:
                if not os.path.isdir(x + y):
                    os.mkdir(x + y)
                elif os.listdir(x + y) != []:
                    dirs_to_clean.append(x + y)
        if dirs_to_clean != []:
            clean_dir_choice = modules.misc.not_empty_folder_choice("data/pictures/*/export/", True)
            if clean_dir_choice:
                for x in dirs_to_clean:
                    shutil.rmtree(x)
                    os.mkdir(x)
                print("Folders cleaned!")
            else:
                raise OSError("OSError: Directory not empty.")
    
    print("Getting compressed picture files ...")
    
    # Get all .gim and .gim.gz files (and invalid ones which are GDPA archives)
    lst_path = "data/dat_files/"
    invalid_lst_path = "data/dat_files/resource/image_charactor.dat/"
    all_gim_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.gim"))]
    all_gim_gz_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.gim.gz"))]
    invalid_gim_files = [y for x in os.walk(invalid_lst_path) for y in glob.glob(os.path.join(x[0], "*.gim"))]
    
    print("Discarding invalid files ...")
    
    # Get invalid GIM filenames
    for i in range(0, len(invalid_gim_files)):
        invalid_gim_files[i] = invalid_gim_files[i].replace("\\", "/").split("/")[-1]
    
    # Remove invalid GIM files
    all_gim_valid = []
    try:
        for x in all_gim_files:
            if x.replace("\\", "/").split("/")[-1] not in invalid_gim_files:
                all_gim_valid.append(x)
    except IndexError:
        pass
    
    print("Copying picture files ...")
    
    # Copy .gim and .gim.gz files
    dest_folder = "data/pictures/gim/export/"
    modules.misc.copy_file_with_tree(all_gim_valid, dat_folder, dest_folder)
    modules.misc.copy_file_with_tree(all_gim_gz_files, dat_folder, dest_folder)
    
    print("Extracting GIM files ...")
    
    # Extract GIM files from GZ archives
    lst_path = "data/pictures/gim/export/"
    all_gim_gz_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.gim.gz"))]
    for x in all_gim_gz_files:
        program_path = "data/tools/gzip/gzip"
        args = ["-d", "-f", "-q", x]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Converting GIM files to PNG files ...")
    
    # Convert GIM to PNG
    lst_path = "data/pictures/gim/export/"
    all_gim_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.gim"))]
    for x in all_gim_files:
        program_path = "data/tools/gimconv/GimConv.exe"
        args = [x.replace("\\", "/"), "-o", x.replace("\\", "/").split("/")[-1].rstrip(".gim") + ".png"]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Exporting pictures into user's folder ...")
    
    # Copy PNG files to their according folder
    all_png_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.png"))]
    modules.misc.copy_file_with_tree(all_png_files, lst_path, "data/pictures/png/export/", False, False)
    modules.misc.copy_file_with_tree(all_png_files, lst_path, "files/pictures/export/", True, False)
    
    return

#########################################################
##  END OF "Strings and pictures extraction functions"  #
#########################################################


def main():
    print("This module should not be run standalone.")
    return


if __name__ == "__main__":
    main()