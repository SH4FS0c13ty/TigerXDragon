# -*- coding: utf-8 -*-
########################################################################
##                            TigerXDragon                            ##
########################################################################
##             extract.py is a part of the PyTTP project.             ##
########################################################################
## extract.py contains all functions used to extract the ISO and .dat ##
## files. It needs to access data/tools/* containing 7-Zip and        ##
## tenoritool to extract everything.                                  ##
########################################################################
## Shell syntax: This script has no shell syntax.                     ##
########################################################################
## Author: SH4FS0c13ty                                                ##
## Copyright: Copyright 2020, TigerXDragon                            ##
## Credits: [SH4FS0c13ty, 123321mario, marcussacana, xyzz]            ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 2.0.0                                                     ##
## Maintainer: SH4FS0c13ty                                            ##
## Email: sh4fs0c13ty@protonmail.com                                  ##
########################################################################

# System modules
import os, glob, shutil, translate.convert.po2txt

# Internal modules
from modules.errors import *
import modules.misc


#######################################
##  ISO and DAT repacking functions  ##
#######################################

def dat_repack(dat_folder):
    """
    Repack all .dat files from .txt files in submitted folder.
    
    :dat_folder: Extracted DAT files folder
    :type dat_folder: str
    
    :return: Nothing
    :rtype: None
    """
    
    print("Renaming seekmap.dat file to seekmap.txt.gz ...")
    
    # Check file existence and rename seekmap.dat file to seekmap.txt.gz
    if not os.path.isfile(dat_folder + "/first/seekmap.dat"):
        raise FileNotFoundError("FileNotFoundError: The following file could not be found: \"" + dat_folder + "/first/seekmap.dat\".")
    shutil.copy(dat_folder + "/first/seekmap.dat", dat_folder + "/seekmap.txt.gz")
    
    print("Extracting the seekmap.txt.gz archive ...")
    
    # Extract seekmap.txt.gz
    program_path = "data/tools/gzip/gzip"
    args = ["-d", "-f", "-q", dat_folder + "/seekmap.txt.gz"]
    modules.misc.execute(program_path, args)
    # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Removing old resource.dat archive ...")
    
    # Remove old resource.dat archive
    if os.path.isfile(dat_folder + "/resource.dat"):
        os.remove(dat_folder + "/resource.dat")
    
    print("Repacking resource.dat archive ...")
    
    # Repack resource.dat archive
    program_path = "data/tools/gpda_handler/gpda_handler"
    args = ["resource.dat.txt"]
    modules.misc.execute(program_path, args, "DEVNULL", dat_folder)
    # modules.misc.execute(program_path, args, "STDOUT", dat_folder) # Show full program output
    
    # Check new resource.dat creation
    if not os.path.isfile(dat_folder + "/resource.dat"):
        raise FileNotFoundError("FileNotFoundError: The resource.dat file could not be repacked.")
    
    print("Generating new seekmap file ...")
    
    # Generate new seekmap file
    program_path = "data/tools/modseekmap/modseekmap"
    args = [dat_folder]
    modules.misc.execute(program_path, args)
    # modules.misc.execute(program_path, args, "STDOUT") # Show full program output

    # Check new seekmap file existence
    if not os.path.isfile(dat_folder + "/seekmap.new"):
        raise FileNotFoundError("FileNotFoundError: The new seekmap file could not be generated.")
    
    # Remove old seekmap.txt file
    if os.path.isfile(dat_folder + "/seekmap.txt"):
        os.remove(dat_folder + "/seekmap.txt")
    
    print("Compressing new seekmap file ...")
    
    # Compress new seekmap file
    program_path = "data/tools/gzip/gzip"
    args = ["-n9", dat_folder + "/seekmap.new"]
    modules.misc.execute(program_path, args)
    # modules.misc.execute(program_path, args, "STDOUT") # Show full program output

    # Check new compressed seekmap existence
    if not os.path.isfile(dat_folder + "/seekmap.new.gz"):
        raise FileNotFoundError("FileNotFoundError: The new seekmap file could not be compressed")
    
    # Remove old seekmap.dat file and move the new one
    os.remove(dat_folder + "/first/seekmap.dat")
    shutil.move(dat_folder + "/seekmap.new.gz", dat_folder + "/first/seekmap.dat")
    
    print("Removing old first.dat archive ...")
    
    # Remove old first.dat archive
    if os.path.isfile(dat_folder + "/first.dat"):
        os.remove(dat_folder + "/first.dat")
    
    print("Repacking first.dat archive ...")
    
    # Repack first.dat archive
    program_path = "data/tools/gpda_handler/gpda_handler"
    args = ["first.dat.txt"]
    modules.misc.execute(program_path, args, "DEVNULL", dat_folder)
    # modules.misc.execute(program_path, args, "STDOUT", dat_folder) # Show full program output
    
    # Check new resource.dat creation
    if not os.path.isfile(dat_folder + "/first.dat"):
        raise FileNotFoundError("FileNotFoundError: The first.dat file could not be repacked.")

    return


def iso_repack(new_iso_file, iso_extracted_folder):
    """
    Repack the ISO file from the extraction folder.

    :new_iso_file: New ISO filename (and path)
    :type iso_file: str
    :extracted_folder: Folder where the ISO file have been extracted
    :type extracted_folder: str

    :return: Nothing
    :rtype: None
    """
    
    # Check files and folders existence
    if not os.path.isfile(iso_extracted_folder + "/PSP_GAME/USRDIR/first.dat") or not os.path.isfile(iso_extracted_folder + "/PSP_GAME/USRDIR/resource.dat"):
        raise FileNotFoundError("FileNotFoundError: The folder \"" + iso_extracted_folder + "\" does not contain all extracted files.")
    elif os.path.isfile(new_iso_file):
        remove_file_choice = modules.misc.existing_file_choice(new_iso_file)
        if remove_file_choice:
            os.remove(new_iso_file)
            print("File removed!")
        else:
            raise OSError("OSError: File already exists.")

    
    print("Repacking ISO file ...")
    
    # Repack ISO file
    program_path = "data/tools/mkisofs/mkisofs"
    args = ["-o", new_iso_file, "-A", "PSP GAME", "-p", "mkisofs", "-publisher", "TigerXDragon v2", "-volid", "Toradora! Portable", "-sysid", "PSP GAME", "-iso-level", "4", iso_extracted_folder.rstrip("/")]
    modules.misc.execute(program_path, args, "DEVNULL")
    # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    # Check new ISO existence
    if not os.path.isfile(new_iso_file):
        raise FileNotFoundError("FileNotFoundError: The ISO file could be repacked.")
    
    return

################################################
##  END OF "ISO and DAT repacking functions"  ##
################################################


################################################
##  Strings and pictures injection functions  ##
################################################

def po2txt_list(all_files, dest_folder):
    """
    Convert PO files to TXT files.

    :all_files: List of PO files to convert
    :type all_files: list
    :dest_folder: Destination folder for converted files
    :type dest_folder:
    
    :return: Nothing
    :rtype: None
    """

    # Check arguments
    if type(all_files) != list:
        raise ArgumentTypeError
    elif not os.path.isdir(dest_folder):
        raise FileNotFoundError("FileNotFoundError: The destination folder could not be found.")

    # Convert TXT to PO
    for x in all_files:
        x = x.replace("\\", "/")
        # print("Converting", x, "...") # Show full program output
        if not os.path.isfile(x):
            raise FileNotFoundError("FileNotFoundError: PO file not found \"" + x + "\".")
        translate.convert.po2txt.main(["--progress=none", "--errorlevel=exception", x, dest_folder + x.split("/")[-1].rstrip(".po") + ".txt"])
        sys.stdout.flush()
        if not os.path.isfile(dest_folder + x.split("/")[-1].rstrip(".po") + ".txt"):
            raise FileNotFoundError("FileNotFoundError: Error while translating \"" + x + "\"")
    
    return


def inject_strings(dat_folder):
    """
    Inject the strings into extracted DAT files.

    :dat_folder: DAT extraction folder
    :type dat_folder: str
    
    :return: Nothing
    :rtype: None
    """

    # Check folders existence
    if not os.path.isdir(dat_folder + "/resource/"):
        raise FileNotFoundError("FileNotFoundError: DAT files have not been extracted.")
    else:
        str_dirs = ["data/strings/obj/", "data/strings/po/", "data/strings/txt/"]
        str_subdirs = ["import/"]
        dirs_to_clean = []
        for x in str_dirs:
            if not os.path.isdir(x):
                os.mkdir(x)
            for y in str_subdirs:
                if not os.path.isdir(x + y):
                    os.mkdir(x + y)
                elif os.listdir(x + y) != []:
                    dirs_to_clean.append(x + y)
        if dirs_to_clean != []:
            clean_dir_choice = modules.misc.not_empty_folder_choice("data/strings/*/import/", True)
            if clean_dir_choice:
                for x in dirs_to_clean:
                    shutil.rmtree(x)
                    os.mkdir(x)
                print("Folders cleaned!")
            else:
                raise OSError("OSError: Directory not empty.")
    
    print("Importing PO files ...")
    
    # Get all .po files
    lst_path = "files/strings/import/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.po"))]
    
    # Copy all PO files
    for i in range(0, len(all_files)):
        shutil.copy(all_files[i], "data/strings/po/import/" + all_files[i].replace("\\", "/").split("/")[-1])
        all_files[i] = "data/strings/po/import/" + all_files[i].replace("\\", "/").split("/")[-1]
    
    print("Converting PO files to TXT files ...")
    
    # Convert PO files to TXT files
    po2txt_list(all_files, "data/strings/txt/import/")
    
    print("Checking all strings in TXT files ...")
    
    # Get all .txt files
    lst_path = "data/strings/txt/import/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.txt"))]
    
    # Check all strings in TXT files
    bad_string_flag = False
    for x in all_files:
        x = x.replace("\\", "/")
        # print("Checking " + str(x) + " ...")# Show full program output
        try:
            modules.misc.newline_corrector(x)
        except InvalidStringError as e:
            print("A problem occured with the following file: " + str(x))
            print(e)
            bad_string_flag = True
    
    # Check for bad_string_flag flag
    if bad_string_flag:
        print("Please correct the above strings before attempting to inject them.")
        print("You can bypass the strings checking block by commenting out line 280 in the repack.py module.")
        raise InvalidStringError
    
    print("Extracting OBJ files from extracted DAT files ...")
    
    # Getting all .obj.gz files
    lst_path = dat_folder + "/resource/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.obj.gz"))]
    
    # Copying compressed string files
    for i in range(0, len(all_files)):
        shutil.copy(all_files[i], "data/strings/obj/import/")
        all_files[i] = "data/strings/obj/import/" + all_files[i].replace("\\", "/").split("/")[-1]
    
    # List of invalid OBJ files
    invalid_obj = [
        "_0011AAA0.obj",
        "_0012AAA0.obj",
        "_0021aaa0.obj",
        "_0022aaa0.obj",
        "_0031aaa0.obj",
        "_0033aaa0.obj",
        "_0035aaa0.obj",
        "_a041aaa0.obj",
        "_a043aaa0.obj",
        "_a052aaa0.obj",
        "_a054aaa0.obj",
        "_a055aaa0.obj",
        "_a192aaa0.obj",
        "_a291aaa0.obj",
        "_a361aaa0.obj",
        "_a362aaa0.obj",
        "_b041aaa0.obj",
        "_b045aaa0.obj",
        "_b062aaa0.obj",
        "_b192aaa0.obj",
        "_c041aaa0.obj",
        "_c042aaa0.obj",
        "_c051aaa0.obj",
        "_c053aaa0.obj",
        "_c054aaa0.obj",
        "_c056aaa0.obj",
        "_c061aaa0.obj",
        "_c064aaa0.obj",
        "_c065aaa0.obj",
        "_c072aaa0.obj",
        "_d021aaa0.obj",
        "STARTPOINT.obj"
    ]
    
    # Remove invalid OBJ files
    for x in invalid_obj:
        os.remove("data/strings/obj/import/" + x + ".gz")
    
    # Extract OBJ files from GZ archives
    for x in all_files:
        program_path = "data/tools/gzip/gzip"
        args = ["-d", "-f", "-q", x]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Patching OBJ files ...")
    
    # Getting all .obj extracted files
    lst_path = "data/strings/obj/import/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.obj"))]
    
    # Patch OBJ files
    for x in all_files:
        x = x.replace("\\", "/")
        if os.path.isfile("data/strings/txt/import/" + x.split("/")[-1].rstrip(".obj") + ".txt"):
            # print("Patching", str(x), "...") # Show full program output
            program_path = "data/tools/objeditor/OBJEGUI.exe"
            args = ["-patch", x, "data/strings/txt/import/" + x.split("/")[-1].rstrip(".obj") + ".txt"]
            modules.misc.execute(program_path, args)
            # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Compressing OBJ files ...")
    
    # Compress OBJ files
    for x in all_files:
        program_path = "data/tools/gzip/gzip"
        args = ["-n9", "-k", x]
        # modules.misc.execute(program_path, args)
        modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Copying compressed OBJ files into extracted DAT folder ...")
    
    # Getting all .obj.gz files
    lst_path = dat_folder + "/resource/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.obj.gz"))]
    
    for x in all_files:
        x = x.replace("\\", "/")
        if os.path.isfile("data/strings/obj/import/" + x.split("/")[-1]):
            # print("Moving", str(x), "...") # Show full program output
            os.remove(x)
            shutil.move("data/strings/obj/import/" + x.split("/")[-1], x)
    
    print("Importing and injecting special files ...")

    # Get all first.dat translatable files
    all_files = ["charaname.txt", "ItemList.csv", "placename.txt", "utf8.txt", "utf16.txt"]
    if os.path.isdir("files/strings/import/first/"):
        lst_path = "files/strings/import/first/"
        all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*."))]
        for x in all_files:
            if os.path.isfile(x):
                # print("Compressing", x, "...") # Show full program output
                # Compress file
                program_path = "data/tools/gzip/gzip"
                args = ["-n9", "-k", "files/strings/import/first/" + x]
                modules.misc.execute(program_path, args)
                # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
                # Remove old special file
                os.remove("data/dat_files/first/text.dat/" + x + ".gz")
                # print("Moving", x, "...") # Show full program output
                try:
                    shutil.move("files/strings/import/first/" + x + ".gz", "data/dat_files/first/text.dat/" + x + ".gz")
                except Exception:
                    raise FileNotFoundError("FileNotFoundError: An error occured while extracting special files.")
    
    return


def inject_pictures(dat_folder):
    """
    Inject the pictures into extracted DAT files.
    
    :dat_folder: DAT extraction folder
    :type dat_folder: str
    
    :return: Nothing
    :rtype: None
    """
    
    # Check folders existence
    if not os.path.isdir(dat_folder + "/resource/"):
        raise FileNotFoundError("FileNotFoundError: DAT files have not been extracted.")
    else:
        str_dirs = ["data/pictures/gim/", "data/pictures/png/"]
        str_subdirs = ["import/"]
        dirs_to_clean = []
        for x in str_dirs:
            if not os.path.isdir(x):
                os.mkdir(x)
            for y in str_subdirs:
                if not os.path.isdir(x + y):
                    os.mkdir(x + y)
                elif os.listdir(x + y) != []:
                    dirs_to_clean.append(x + y)
        if dirs_to_clean != []:
            clean_dir_choice = modules.misc.not_empty_folder_choice("data/pictures/*/import/", True)
            if clean_dir_choice:
                for x in dirs_to_clean:
                    shutil.rmtree(x)
                    os.mkdir(x)
                print("Folders cleaned!")
            else:
                raise OSError("OSError: Directory not empty.")
    
    print("Importing PNG files ...")
    
    # Get all .png files
    lst_path = "files/pictures/import/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.png"))]
    
    # Check for files in DAT extracted folder
    all_valid_files = []
    for x in all_files:
        x = x.replace("\\", "/")
        if os.path.isfile(dat_folder + x[len(lst_path)-1:-4] + ".gim") or os.path.isfile(dat_folder + x[len(lst_path)-1:-4] + ".gim.gz"):
            # print("Importing", str(x), "...") # Show full program output
            all_valid_files.append(x)
    
    # Copy pictures and tree
    modules.misc.copy_file_with_tree(all_valid_files, lst_path, "data/pictures/png/import/", False, False)
    
    print("Compressing PNG files ...")
    
    # Get all .png files
    lst_path = "data/pictures/png/import/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.png"))]
    
    # Compress PNG files
    for x in all_files:
        x = x.replace("\\", "/")
        # print("Compressing", str(x), "...") # Show full program output
        program_path = "data/tools/pngquant/pngquant"
        args = ["--skip-if-larger", "--force", "--ext=.png", "--quality=50-90", x]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Converting PNG files to GIM files ...")
    
    # Convert PNG files to GIM files
    for x in all_files:
        x = x.replace("\\", "/")
        print("Converting", str(x), "...") # Show full program output
        program_path = "data/tools/gimconv/GimConv.exe"
        args = [x, "--format_style", "psp", "--format_endian", "little", "--pixel_order", "normal", "--image_format", "index8", "-o", x.split("/")[-1][:-4] + ".gim"]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output

    print("Copying GIM files ...")
    
    # Get all .gim files
    lst_path = "data/pictures/png/import/"
    all_files = [y for x in os.walk(lst_path) for y in glob.glob(os.path.join(x[0], "*.gim"))]
    
    # Copy pictures and tree
    modules.misc.copy_file_with_tree(all_files, lst_path, "data/pictures/gim/import/", False, False)
    
    print("Compressing GIM files ...")
    
    # Compress GIM files
    for x in all_files:
        x = x.replace("\\", "/")
        # print("Compressing", str(x), "...") # Show full program output
        program_path = "data/tools/gzip/gzip"
        args = ["-n9", x]
        modules.misc.execute(program_path, args)
        # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    print("Copying picture files into extracted DAT folder ...")
    
    # Get all .gim and .gim.gz files from extracted DAT folder
    all_ext_files = [y for x in os.walk(dat_folder) for y in glob.glob(os.path.join(x[0], "*.gim"))]
    all_ext_gz_files = [y for x in os.walk(dat_folder) for y in glob.glob(os.path.join(x[0], "*.gim.gz"))]
    
    # Moving .gim files into extracted DAT folder
    for x in all_ext_files:
        x = x.replace("\\", "/")
        if os.path.isfile(lst_path + x[len(dat_folder)-1:] + ".gz"):
            # print("Moving", str(x), "...") # Show full program output
            os.remove(x)
            shutil.move(lst_path + x[len(dat_folder)-1:] + ".gz", x)
    
    # Moving .gim.gz files into extracted DAT folder
    for x in all_ext_gz_files:
        x = x.replace("\\", "/")
        if os.path.isfile(lst_path + x[len(dat_folder)-1:]):
            # print("Moving", str(x), "...") # Show full program output
            os.remove(x)
            shutil.move(lst_path + x[len(dat_folder)-1:], x)
    
    return


#########################################################
##  END OF "Strings and pictures injection functions"  ##
#########################################################


def main():
    print("This module should not be run standalone.")
    return


if __name__ == "__main__":
    main()