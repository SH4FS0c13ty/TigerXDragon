# -*- coding: utf-8 -*-
########################################################################
##                             SH4 Syscon                             ##
########################################################################
##                syscon.py is a part of SH4's Toolkit                ##
########################################################################
## syscon.py is used to handle console window in both Windows and     ##
## Linux system like modifying window title, clearing console, etc .. ##
## This module is not standalone and should be used as a submodule.   ##
########################################################################
## Shell syntax: This module has no shell syntax.                     ##
########################################################################
## Author: SH4FS0c13ty                                                ##
## Copyright: Copyright 2020, SH4's Toolkit                           ##
## Credits: [SH4FS0c13ty]                                             ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 1.0.0                                                     ##
## Repository: https://gitlab.com/SH4FS0c13ty/SH4s_Toolkit            ##
## Maintainer: SH4FS0c13ty                                            ##
## Email: sh4fs0c13ty@protonmail.com                                  ##
########################################################################

# System modules
import os, sys, platform


def clear_con():
    """
    Clear the console.
    
    :return: Nothing
    :rtype: None
    """
    
    # Choose which command to use according to OS
    if platform.system() == "Windows":
        clear_cmd = "cls"
    else:
        clear_cmd = "clear"
    
    # Clear the console
    try:
        os.system(clear_cmd)
    except OSError as e:
        print(e)
    
    return


def set_con_title(con_title):
    """
    Set console title.
    
    :con_title: Title to set
    :type con_title: str
    
    :return: Nothing
    :rtype: None
    """
    
    # Set title as string
    con_title = str(con_title)
    
    # Choose which command to use according to OS
    if platform.system() == "Windows":
        os.system("title " + con_title)
    else:
        sys.stdout.write("\x1b]2;" + con_title + "\x07")
    
    return


def main():
    print("This module should not be run standalone.")
    return

if __name__ == "__main__":
    main()