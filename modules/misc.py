# -*- coding: utf-8 -*-
########################################################################
##                            TigerXDragon                            ##
########################################################################
##           misc.py is a part of the TigerXDragon project.           ##
########################################################################
## misc.py is a module listing several different functions needed by  ##
## TigerXDragon but don't need a dedicated module. This allows to     ##
## reduce the number of modules and interconnections between them.    ##
########################################################################
## Shell syntax: This module has no shell syntax.                     ##
########################################################################
## Author: SH4FS0c13ty                                                ##
## Copyright: Copyright 2020, TigerXDragon                            ##
## Credits: [SH4FS0c13ty, 123321mario, marcussacana, xyzz]            ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 2.0.0                                                     ##
## Repository: https://gitlab.com/SH4FS0c13ty/TigerXDragon            ##
## Maintainer: SH4FS0c13ty                                            ##
## Email: sh4fs0c13ty@protonmail.com                                  ##
########################################################################

# System modules
import os, subprocess, platform, shutil

# Internal modules
from modules.errors import *


##########################
##  Execution function  ##
##########################

def execute(prog_path, arguments, target_output="DEVNULL", working_dir="./"):
    """
    Execute a program using subprocess cross-platformly and wait for the process to finish.
    
    :prog_path: Path of the program without the program extension.
    :type prog_path: str
    :arguments: List of arguments to pass
    :type arguments: list
    :target_output: Target output (like STDOUT or DEVNULL)
    :type target_output: str
    
    :return: Nothing
    :rtype: None
    """
    
    # Check arguments
    if type(prog_path) != str or type(arguments) != list or type(target_output) != str:
        raise ArgumentTypeError
    elif target_output.upper() != "STDOUT" and target_output.upper() != "DEVNULL":
        raise ArgumentTypeError("Output target is not STDOUT or DEVNULL.")
    elif not os.path.isdir(working_dir):
        raise FileNotFoundError("FileNotFoundError: Submitted working directory could not be found.")
    
    # Variables for path and wine use
    in_path = False
    wine_use = False
    
    # Check if wine is necessary
    if not prog_path.endswith(".exe"):
        # Check platform and program path
        if platform.system() == "Windows":
            prog_path += ".exe"
        else:
            # Check if program is in path variable
            try:
                subprocess.check_output("which " + prog_path.split("/")[-1], shell=True)
                in_path = True
                prog_path = prog_path.split("/")[-1]
            except subprocess.CalledProcessError:
                prog_path += ".bin"
    else:
        if platform.system() == "Linux":
            wine_use = True
    
    if not os.path.isfile(prog_path) and not in_path:
        raise FileNotFoundError("FileNotFoundError: The following program could not be found: " + str(prog_path))
    
    # Insert program path as first argument (or wine if needed)
    arguments.insert(0, prog_path)
    if wine_use:
        arguments.insert(0, "wine")
    
    # Launch process according to target_output argument (bypass the bad file descriptor subprocess.STDOUT problem)
    if target_output == "DEVNULL":
        prog_process = subprocess.Popen(arguments, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL, cwd=working_dir)
    elif target_output == "STDOUT":
        prog_process = subprocess.Popen(arguments, cwd=working_dir)
    
    # Wait for the process to finish
    prog_process.wait()

    return

###################################
##  END OF "Execution function"  ##
###################################


#########################
##  Choices functions  ##
#########################

def not_empty_folder_choice(folder, bypass_check=False):
    """
    Let the user choose to clean the folder or not.
    
    :folder: Folder to clean
    :type folder: str
    :bypass_check: Bypass folder existence checking
    :type bypass_check: bool
    
    :return: True or False
    :rtype: bool
    """
    
    # Check argument
    if not os.path.isdir(folder) and not bypass_check:
        raise FileNotFoundError
    
    choice = ""
    attended_response = ["y", "yes", "n", "no"]
    
    print("The folder \"" + str(folder) + "\" is not empty.")
    print("Please note that if the folder is not empty, TigerXDragon will not start the process.")
    
    while choice.lower() not in attended_response:
        choice = input("Do you want to clean it (y/n)? ")
    
    if choice == "y" or choice == "yes":
        return True
    else:
        return False
    
    return


def existing_file_choice(file, bypass_check=False):
    """
    Let the user choose to remove the file or not.
    
    :file: File to remove
    :type file: str
    :bypass_check: Bypass folder existence checking
    :type bypass_check: bool
    
    :return: True or False
    :rtype: bool
    """
    
    # Check argument
    if not os.path.isfile(file) and not bypass_check:
        raise FileNotFoundError
    
    choice = ""
    attended_response = ["y", "yes", "n", "no"]
    
    print("The file \"" + str(file) + "\" does already exist.")
    print("Please note that if the file is not removed, TigerXDragon will not start the process.")
    
    while choice.lower() not in attended_response:
        choice = input("Do you want to remove it (y/n)? ")
    
    if choice == "y" or choice == "yes":
        return True
    else:
        return False
    
    return

##################################
##  END OF "Choices functions"  ##
##################################


######################
##  Copy functions  ##
######################

def copy_dat_files(dat_files_location, dat_files_target_dir, root_dat_files):
    """
    Copy the first.dat and resource.dat files from their location to the target directory.
    
    :dat_files_location: DAT files location
    :type dat_files_location: str
    :dat_files_target_dir: Target directory for DAT files
    :type dat_files_target_dir: str
    
    :return: Nothing
    :rtype: None
    """
    
    # Check arguments
    if not os.path.isdir(dat_files_location):
        raise FileNotFoundError("FileNotFoundError: Source folder could not be found.")
    elif not os.path.isdir(dat_files_target_dir):
        os.mkdir(dat_files_target_dir)
    for x in root_dat_files:
        if not os.path.isfile(dat_files_location + "/" + x):
            raise FileNotFoundError("FileNotFoundError: DAT files could not be found in the source folder.")
    
    # Copy first.dat and resource.dat files
    for x in root_dat_files:
        shutil.copyfile(dat_files_location + "/" + x, dat_files_target_dir + "/" + x)
    
    return


def copy_file_with_tree(file_list, base_folder, dest_folder, del_original_file=False, add_gz=True):
    """
    Copy the files along with their tree from the submitted list.
    
    :file_list: List of file to copy
    :type file_list: list
    :base_folder: Root folder of the files
    :type base_folder: str
    :dest_folder: Destination folder of the files
    
    
    :return: Nothing
    :rtype: None
    """
    
    # Check argument and file existence
    if type(file_list) != list:
        raise ArgumentTypeError
    elif not os.path.isdir(base_folder) or not os.path.isdir(dest_folder):
        raise FileNotFoundError("FileNotFoundError: The base or destination folder could not be found.")
    for i in range(0, len(file_list)):
        file_list[i] = file_list[i].replace("\\", "/")
        if not os.path.isfile(file_list[i]):
            raise FileNotFoundError("FileNotFoundError: The following file could not be found: " + str(file_list[i]))
    
    # Create folder tree and copy files
    base_folder_list = base_folder.split("/")
    for x in file_list:
        x_folder = ""
        for y in x.split("/"):
            if y not in base_folder_list and y != x.split("/")[-1]:
                x_folder += y + "/"
                if not os.path.isdir(dest_folder + x_folder):
                    os.mkdir(dest_folder + x_folder)
        if not x.split("/")[-1].endswith(".gz") and add_gz:
            x_target = x.split("/")[-1] + ".gz"
        else:
            x_target = x.split("/")[-1]
        shutil.copy(x, dest_folder + x_folder + x_target)
        if del_original_file:
            os.remove(x)
    
    return

###############################
##  END OF "Copy functions"  ##
###############################


#######################
##  Xdelta function  ##
#######################

def create_xdelta_patch(original_iso, translated_iso, patch_filename):
    """
    Create an Xdelta patch by comparing the 2 ISO files.

    :original_iso: Original JP or EN ISO (or any other language you translate from)
    :type original_iso: str
    :translated_iso: Translated ISO
    :type translated_iso: str
    :patch_filename: New patch filename (and path)
    :type patch_filename: str

    :return: Nothing
    :rtype: None
    """
    
    # Check arguments
    if not os.path.isfile(original_iso):
        raise FileNotFoundError("FileNotFoundError: The original ISO file could not be found.")
    elif not os.path.isfile(translated_iso):
        raise FileNotFoundError("FileNotFoundError: The translated ISO file could not be found.")
    elif os.path.isfile(patch_filename):
        remove_file_choice = existing_file_choice(patch_filename)
        if remove_file_choice:
            os.remove(patch_filename)
            print("File removed!")
        else:
            raise OSError("OSError: File already exists.")
    
    print("Generating the xdelta patch ...")
    
    # Generate xdelta patch
    program_path = "data/tools/xdelta/xdelta"
    args = ["-e", "-s", original_iso, translated_iso, patch_filename]
    execute(program_path, args, "DEVNULL")
    # modules.misc.execute(program_path, args, "STDOUT") # Show full program output
    
    # Check new xdelta patch existence
    if not os.path.isfile(patch_filename):
        raise FileNotFoundError("FileNotFoundError: The xdelta patch file could not be created.")
    
    return

################################
##  END OF "Xdelta function"  ##
################################


##################################
##  String correction function  ##
##################################

def newline_corrector(txt_file):
    """
    Correct the position of the _ character according to a 45 characters limit.
    
    :txt_file: Text file to submit
    :type txt_file: str
    
    :return: Operation success and status code
    :rtype: tuple
    """

    # Check file existence and extension
    if not os.path.isfile(txt_file):
        raise FileNotFoundError("FileNotFoundError: The following file could not be found: \"" + str(txt_file) + "\"")
    
    # Open file and get all lines
    with open(txt_file, "r", encoding="utf-8") as file_opened_r:
        all_lines = file_opened_r.readlines()
    
    # Initialize flag and positions
    correction_flag = False
    correction_pos = []
    
    # Do a precheck on all lines
    for i in range(0, len(all_lines)):
        if len(all_lines[i]) > 45*3:
            raise InvalidStringError("String too long: " + all_lines[i])
        x_list = all_lines[i].split("_")
        for y in x_list:
            if len(y) > 47:
                correction_flag = True
                correction_pos.append(i)
    
    # Correct lines
    if correction_flag:
        for x in correction_pos:
            tmp_line = all_lines[x]
            tmp_line = tmp_line.replace("_", " ")
            j = 0
            try:
                for i in range(0, 3):
                    j += 45
                    if tmp_line[j] != " ":
                        while tmp_line[j] != " ":
                            j -= 1
                    tmp_line = tmp_line[:j] + "_" + tmp_line[j+1:]
                        
            except IndexError:
                pass
            all_lines[x] = tmp_line
        
        # Do a postcheck on all lines
        for i in range(0, len(all_lines)):
            x_list = all_lines[i].split("_")
            for y in x_list:
                if all_lines[i][1:-2] == (all_lines[i][1] * (len(all_lines[i])-3)):
                    break
                elif len(y) > 47:
                    raise InvalidStringError("Newline correction couldn't correct the following string: " + all_lines[i])
        
        # Write modifications in file
        with open(txt_file, "w", encoding="utf-8") as file_opened_w:
            file_opened_w.writelines(all_lines)

    return

###########################################
##  END OF "String correction function"  ##
###########################################


def main():
    print("This module should not be run standalone.")
    return

if __name__ == "__main__":
    main()