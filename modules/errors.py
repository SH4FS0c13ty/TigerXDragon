# -*- coding: utf-8 -*-
########################################################################
##                             SH4 Errors                             ##
########################################################################
##                errors.py is a part of SH4's Toolkit                ##
########################################################################
## errors.py is used to handle custom errors with only one class      ##
## (actually, there are three classes ...). This works using the      ##
## try-except syntax and raise to raise the custom error.             ##
########################################################################
## Shell syntax: This module has no shell syntax.                     ##
########################################################################
## Author: SH4FS0c13ty                                                ##
## Copyright: Copyright 2020, SH4's Toolkit                           ##
## Credits: [SH4FS0c13ty]                                             ##
## License: MIT License (https://opensource.org/licenses/MIT)         ##
## Version: 1.0.0                                                     ##
## Repository: https://gitlab.com/SH4FS0c13ty/SH4s_Toolkit            ##
## Maintainer: SH4FS0c13ty                                            ##
## Email: sh4fs0c13ty@protonmail.com                                  ##
########################################################################

# System modules
import sys, inspect


# Custom errors with according messages
custom_errors = {
    # General errors
    "ArgumentTypeError": "Invalid passed argument type.",
    "InvalidFormatError": "Invalid format for string argument.",
    "InvalidLengthError": "Invalid submitted data length.",
    
    # DB_Handler errors
    "TableNotFoundError": "The table could not be found in the database.",
    "RecordNotFoundError": "The record could not be found in the database.",
    
    # Miscellaneous errors
    "InvalidStringError": "Invalid string detected."
    
}


def process_aliases(aliases_dict):
    """
    Process the aliases dictionary to use them with in the CustomError class.
    
    :aliases_list: Aliases dictionary to submit
    :type aliases_list: dict
    
    :return: List containing a tuple with aliases and a list with according messages
    :rtype: list
    """
    
    # Check argument
    if type(aliases_dict) != dict:
        print("ArgumentTypeError: 'aliases_dict' variable is not dict.")
        sys.exit(-1)
    
    # Initialize list variables
    aliases_list = []
    messages_list = []
    
    # Process aliases with according messages
    for x in aliases_dict:
        aliases_list.append(str(x))
        messages_list.append(str(aliases_dict[x]))
    
    try:
        aliases_tuple = tuple(aliases_list)
    except ValueError as e:
        print(e)
        sys.exit(-1)
    
    return [aliases_tuple, messages_list]
    

def create_error_classes():
    """
    Create error classes.
    
    :return: Nothing
    :rtype: None
    """

    # Base Exception class to inherit
    class Error(Exception):
        """ Base class for other exceptions """
        pass


    # Alias creator class
    class AKA(type):
        """ 'Also Known As' metaclass to create aliases for a class """
        def __new__(cls, classname, bases, attrs):
            class_ = type(classname, bases, attrs)
            globals().update({alias: class_ for alias in attrs.get('aliases', [])})
            return class_


    # Custom Error class
    class CustomError(Error, metaclass=AKA):
        """ CustomError class to handle every custom errors """
        # Get custom aliases and according messages
        global res
        res = process_aliases(custom_errors)
        
        # Class aliases (custom error names)
        aliases = res[0]
        
        def __init__(self, message="An unknown error occured. Please contact the developer."):
            # Get alias index to get according messages
            used_alias = ""
            stack = inspect.stack()
            for x in stack[1][0].f_code.co_names:
                if x in custom_errors:
                    used_alias = x
                    break
             
            # Get according message
            global res
            for i in range(0, len(res[0])):
                if res[0][i] == used_alias:
                    modified_message = True
                    for x in ["An unknown error occured. Please contact the developer.", res[1][i]]:
                        if message == x:
                            modified_message = False
                            break
                    if modified_message:
                        message = res[0][i] + ": " + message
                    else:
                        message = res[0][i] + ": " + res[1][i]
                    
                    break
            
            self.message = message
            super().__init__(self.message)
        
        def __str__(self):
            if self.message:
                return self.message
            else:
                return "An unknown error occured. Please contact the developer."

    return


def main():
    print("This module should not be used standalone.")
    sys.exit(0)


if __name__ == "__main__":
    main()

create_error_classes()
print("error modules loaded successfully")