# -*- coding: utf-8 -*-
######################################################################
#                           SH4 DB Handler                           #
######################################################################
#              db_handler.py is a part of SH4's Toolkit              #
######################################################################
# db_handler.py is used to handle database operations like creating, #
# reading, writing, querying, ... Databases are used to keep many    #
# information like settings, languages, error descriptions, ...      #
######################################################################
# Shell syntax: db_handler.py                                        #
######################################################################
# Author: SH4FS0c13ty                                                #
# Copyright: Copyright 2020, SH4's Toolkit                           #
# Credits: [SH4FS0c13ty]                                             #
# License: MIT License (https://opensource.org/licenses/MIT)         #
# Version: 1.0.0                                                     #
# Repository: https://gitlab.com/SH4FS0c13ty/SH4s_Toolkit            #
# Maintainer: SH4FS0c13ty                                            #
# Email: sh4fs0c13ty@protonmail.com                                  #
######################################################################

# System modules
import os, threading, sqlite3

# Internal module (for error handling)
if os.path.isfile("modules/errors.py"):
    import modules.errors
    errors_module = True
elif os.path.isfile("errors.py"):
    import errors
    errors_module = True
else:
    print("The errors.py module could not be found.")
    errors_module = False

lock = threading.Lock()


def db_connect(db_file, sql_cmd_list):
    """
    Connect to a database using sqlite3.
    
    :db_file: Database file to connect to
    :type db_file: str
    :sql_cmd_list: List of SQL commands to execute
    :type sql_cmd_list: list
    
    :return: SQL results or error code
    :rtype: list or int
    """
    
    # Check arguments type
    if type(sql_cmd_list) != list or type(db_file) != str:
        if errors_module:
            raise ArgumentTypeError
        else:
            print("ArgumentTypeError: Invalid passed argument type.")
            return -1
    
    # Check database existence
    if not os.path.isfile(db_file):
        try:
            with open(db_file, "w") as db_file_write:
                db_file_write.write("")
        except OSError as e:
            print(e)
            return -1
    
    try:
        lock.acquire()
        # Connect to database and set cursor
        conn = sqlite3.connect(db_file, isolation_level=None)
        curs = conn.cursor()
        try:
            all_res = []
            # Execute SQL commands
            for x in sql_cmd_list:
                curs.execute(x)
                
                # Get row names
                row_names = []
                for row in curs.description:
                    row_names.append(row[0])
                
                sql_res = curs.fetchall()
                sql_res.insert(0, tuple(row_names))
                # DEBUG MODE ONLY - Print SQL results
#                if len(sql_res) != 0:
#                    print("Result of \"" + x + "\":")
#                    for y in sql_res:
#                        print(y)
#                    print()
                # END OF DEBUG MODE ONLY
                all_res.append(sql_res)
        except sqlite3.Error as e:
            print(e)
            return -1
        finally:
            conn.close()
            return all_res
    except sqlite3.Error as e:
        print(e)
        return -1
    except OSError as e:
        print(e)
        return -1
    finally:
        lock.release()


def db_read_val(db_file, tab_val_id):
    """
    Read a value in the database.
    
    :db_file: Database file to connect to
    :type db_file: str
    :tab_val_id: Table and value to get
    :type tab_val_id: str
    
    :return: Result or error code
    :rtype: list or int
    """

    # Check type of tab_val_id and db_file
    if type(tab_val_id) != str or type(db_file) != str:
        if errors_module:
            raise ArgumentTypeError
        else:
            print("ArgumentTypeError: Invalid passed argument type.")
            return -1
    
    # Check format of tab_val_id
    tab_val_id = tab_val_id.split(".", 1)
    if len(tab_val_id) < 2:
        if errors_module:
            raise ArgumentTypeError
        else:
            print("InvalidFormatError: Invalid format for tab_val_id.")
            return -1
    
    # Check table existence (from query)
    sql_cmd_list = ["SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%' AND name = '" + tab_val_id[0] + "';"]
    res = db_connect(db_file, sql_cmd_list)
    try:
        if type(res) != list:
            print("Error", str(res), ":", "ERROR_STRING_NOT_FOUND")
            return res
        elif len(res) == 0 or len(res[0]) == 0:
            if errors_module:
                raise TableNotFoundError
            else:
                print("TableNotFoundError: The table could not be found in the database.")
                return -1
    except IndexError as e:
        print(e)
        return -1

    # Read the table
    sql_cmd_list = ["SELECT * FROM " + tab_val_id[0] + " WHERE id = '" + tab_val_id[1] + "';"]
    res = db_connect(db_file, sql_cmd_list)
    if type(res) != list:
        print("Error", str(res), ":", "ERROR_STRING_NOT_FOUND")
        return res
    try:
        if len(res[0]) == 0:
            if errors_module:
                raise RecordNotFoundError
            else:
                print("RecordNotFoundError: The record could not be found in the database.")
                return -1
    except IndexError as e:
        print(e)
        return -1
    return res

def db_write_val(db_file, tab_val_id, cols, data, create_if_not_present=False):
    """
    Write a value in the database.
    
    :db_file: Database file to connect to
    :type db_file: str
    :tab_val_id: Table and value to write
    :type tab_val_id: str
    :cols: Columns to write in
    :type cols: list
    :data: Data to write
    :type data: list
    
    :return: Status code
    :rtype: int
    """
    
    # Check type of tab_val_id, db_file, cols, data and create_if_not_present
    if type(tab_val_id) != str or type(db_file) != str or type(cols) != list or type(data) != list or type(create_if_not_present) != bool:
        if errors_module:
            raise ArgumentTypeError
        else:
            print("ArgumentTypeError: Invalid passed argument type.")
            return -1
    
    # Check format of tab_val_id
    tab_val_id_list = tab_val_id.split(".", 1)
    if len(tab_val_id_list) < 2:
        if errors_module:
            raise InvalidFormatError
        else:
            print("InvalidFormatError: Invalid format for tab_val_id")
            return -1
    
    # Check length of cols and data
    if len(cols) != len(data):
        if errors_module:
            raise InvalidLengthError
        else:
            print("InvalidLengthError: Invalid submitted data length.")
            return -1
    
    res_db_read_val = db_read_val(db_file, tab_val_id)
    if res_db_read_val == 3 and create_if_not_present == True:
        # Generate columns field
        columns = ""
        for x in cols:
            if columns != "":
                columns += ", "
            columns += x

        # Generate values field
        values = ""
        for x in data:
            if values != "":
                values += ", "
            values += "'" + x + "'"
            
        sql_cmd_list = [
            "INSERT INTO " + tab_val_id_list[0] + "(" + columns + ") VALUES(" + values + ");"
        ]
        res = db_connect(db_file, sql_cmd_list)
    elif type(res_db_read_val) == list:
        # Generate set field
        columns_vals = ""
        for i in range (0, len(data)):
            if columns_vals != "":
                columns_vals += ", "
            columns_vals += cols[i] + " = '" + data[i] + "'"
        
        sql_cmd_list = [
            "UPDATE " + tab_val_id_list[0] + " SET " + columns_vals + " WHERE id = '" + tab_val_id_list[1] + "';"
        ]
        res = db_connect(db_file, sql_cmd_list)
    else:
        print("Could not create the new record.")
        return res_db_read_val
    
    try:
        if len(res) == 0 or len(res[0]) == 0:
            return 0
        else:
            return res
    except IndexError as e:
        print(e)
        return res


def sql_prompt():
    """
    An SQL command prompt wrapper to handle errors.
    
    :return: Nothing
    :rtype: None
    """
    
    com = ""
    database = ""
    while com.lower() != "exit;" and com.lower() != "quit;":
        try:
            sql_cmd_list = None
            com = str(input("DB Handler (SQL)> "))
            while not com.endswith(";"):
                com += " " + input("SQL query> ")
            com = ' '.join(com.split())
            if com.upper() == "SHOW TABLES;" or com.upper() == "SHOW TABLES ;":
                sql_cmd_list = ["SELECT name FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%';"]
            if com.split()[0].upper() == "USE" and (len(com.split()) == 2 or len(com.split()) == 3):
                if os.path.isfile(com.split()[1].rstrip(";")):
                    database = com.split()[1].rstrip(";")
                else:
                    raise FileNotFoundError("FileNotFoundError: The database could not be found.")
            elif com.split()[0].lower().rstrip(";") == "exit" or com.split()[0].lower().rstrip(";") == "quit":
                break
            else:
                if type(sql_cmd_list) != list:
                    sql_cmd_list = [com]
            if type(sql_cmd_list) == list:
                if not os.path.isfile(database):
                    raise FileNotFoundError("FileNotFoundError: No database selected or non existent.")
                else:
                    try:
                        res = db_connect(database, sql_cmd_list)
                    except OSError as e:
                        print("An error occured with type OSError:")
                        print(e)
                    except sqlite3.Error as e:
                        print("An error occured with type SQLite3Error:")
                        print(e)
                    if type(res) != list:
                        print("An error occured:")
                        print("Error", str(res))
                    else:
                        if len(res) != 0:
                            res_beautiful = sql_result_beautifier(res)
                            for x in res_beautiful:
                                print(x)
                            print()
                        else:
                            print("No result.")
        except Exception as e:
            print(e)
            pass
    
    return
            
def sql_result_beautifier(sql_res):
    """
    Beautify SQL result.

    :sql_res: SQL result to submit
    :type sql_res: list

    :return: Beautified SQL result or error code
    :return: list or int
    """

    # Check argument type and format
    if type(sql_res) != list:
        if errors_module:
            raise ArgumentTypeError
        else:
            return -1
    elif len(sql_res) == 0:
        if errors_module:
            raise ArgumentTypeError
        else:
            return -2
    for x in sql_res:
        if type(x) != list:
            if errors_module:
                raise ArgumentTypeError
            else:
                return -3

    # Initalize beautifier variables
    col_width = []
    table = [""]

    # Just some loop help for the first pass
    # x = [(example),(example)]
    # i = (example)
    # j = example
    
    # First pass to get new table dimensions
    for x in sql_res:
        for i in range(0, len(x)):
            for j in range(0, len(x[i])):
                if i == 0:
                    col_width.append(0)
                try:
                    if len(x[i][j]) > col_width[j]:
                        col_width[j] = len(x[i][j])
                except TypeError:
                    if 4 > col_width[j]:
                        col_width[j] = 4
    
    # Generate table separators
    for j in range(0, len(col_width)):
        table[0] += ("+")
        for k in range(0, col_width[j]+2):
            table[0] += "-"
    table[0] += ("+")
    
    # Generate table body
    for i in range(0, len(sql_res[0])):
        table.append("| ")
        for j in range(0, len(sql_res[0][i])):
            table[i+1] += str(sql_res[0][i][j])
            for k in range(len(str(sql_res[0][i][j])), col_width[j]+1):
                table[i+1] += " "
            table[i+1] += "| "
    
    # Insert separators
    table.insert(2, table[0])
    table.append(table[0])
    
    return table
    
    
    

def main():
    sql_prompt()
    return


    
if __name__ == "__main__":
    main()